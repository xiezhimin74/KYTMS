package com.kytms.carrier.dao;

import com.kytms.core.dao.BaseDao;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 * 承运商设置DAO层
 * @author 陈小龙
 * @create 2018-01-15
 */
public interface CarrierDao<Carrier> extends BaseDao<Carrier> {
}
